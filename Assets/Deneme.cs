﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Car{

    public bool isRunning = false;
    public float gas = 100f;
    public int capacity = 5;
    public float enginePower = 500f;

    public void AddGas(float g){
        gas+=g;
    }

    public void Drive(){
        if (isRunning){
            gas -= enginePower*0.005f;
            if (gas<=0f){
                Stop();
            }
        }else{
            MonoBehaviour.print("Önce motoru çalıştır");
        }
    }

    public void Start(){
        isRunning = true;
    }
    public void Stop(){
        isRunning = false;
    }

    public void Steer(){

    }

    public void Brake(){

    }

}

public class Date{
    public static int year = 1200;
    public static string day;
}


public class Math{
    public static float DurationToReachDestination(float destination,float velocity){
        return destination/velocity;
    }

    public static float CalculateVelocity(float destination,float duration){
        return destination / duration;
    }

    public static float CalculateDestination(float duration,float velocity){
        return duration * velocity;
    }
}
[System.Serializable]
public class Student: Human{
    public int grades;

    public Student(string name,int age,int grades){
        this.name = name;
        this.age = age;
        this.grades = grades;
        
    }

    public override void WakeUp(){
        base.WakeUp();
        MonoBehaviour.print("Yine bir okul günü!");
    }
}
[System.Serializable]
public class Teacher: Human{
    public float salary;
    public string branch;
    public override void WakeUp(){
        MonoBehaviour.print("Merhaba Dünya!");
    }
}

public class Human{
    public string name;
    public int age;
    public virtual void WakeUp(){
        MonoBehaviour.print("Günaydın! Dedi " + name);
    }
}
[System.Serializable]
public class Principal : Teacher{
    public int managementSkill;
    public override void WakeUp(){
        base.WakeUp();
        MonoBehaviour.print("Gidelim öğrencilerin saçına karışalım");
    }
}