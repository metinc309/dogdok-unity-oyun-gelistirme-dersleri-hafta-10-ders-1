﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    public Student student1 = new Student("John",14,100);
    public Teacher teacher1 = new Teacher();
    public Principal principal1 = new Principal();

    // Start is called before the first frame update
    void Start()
    {
        Date.year += 2;
    }




    // Update is called once per frame
    void Update()
    {

        if(Input.GetKeyDown(KeyCode.Q)){
            student1.WakeUp();
            teacher1.WakeUp();
            principal1.WakeUp();
        }
    }
}
