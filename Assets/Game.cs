﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using TMPro;
public class Game : MonoBehaviour
{
    public int point;
    public TextMeshProUGUI pointText;
    public GameObject healthbar;
    public float health;
    public float maxHealth;

    // Start is called before the first frame update
    void Start()
    {
        health=maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        point+=1;
        

        healthbar.transform.localScale = new Vector3(health/maxHealth,1,1);

        if (health>0){
            health-=0.5f;
        }

        pointText.text = "Puan: "+point;
    }
}
